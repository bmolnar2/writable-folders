package com.snapsoft;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;

public class TreeItem {
	String name;
	List<TreeItem> children;

	TreeItem(String name) {
		this.name = name;
		this.children = new ArrayList<TreeItem>();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof TreeItem && ((TreeItem) obj).name.equals(this.name);
	}

	public static JSONObject toJSON(TreeItem root, List<TreeItem> childNodes) {
		JSONObject json = new JSONObject();
		List<JSONObject> children = new ArrayList<>();
		if (childNodes != null && !childNodes.isEmpty()) {
			for (TreeItem subnode : childNodes) {
				children.add(toJSON(subnode, subnode.children));
			}
		}
		json.put("children", children);
		json.put("name", root.name);
		return json;
	}

	public static TreeItem getWritableFolderStructure(List<String> readableFolders, List<String> writableFolders) {
		List<String> accessibleFolders = new ArrayList<>();
		TreeItem root = new TreeItem("/");
		writableFolders.forEach(folder -> {
			String path = folder;
			while (path.split("/").length > 1) {
				if (writableFolders.contains(path) || readableFolders.contains(path)) {
					path = path.substring(0, path.lastIndexOf("/"));
				} else {
					return;
				}
			}
			accessibleFolders.add(folder);
		});
		accessibleFolders.forEach(folder -> buildTreeFromFolderList(folder, root));
		return root;
	}

	private static TreeItem buildTreeFromFolderList(String path, TreeItem root) {
		List<String> subPaths = Arrays.asList(path.split("/")).stream().filter(sub -> !sub.isEmpty())
				.collect(Collectors.toList());
		if (subPaths != null && subPaths.size() > 0) {
			TreeItem leaf = new TreeItem(subPaths.get(0));
			if (root.children.contains(leaf)) {
				buildTreeFromFolderList(String.join("/", subPaths.subList(1, subPaths.size())),
						root.children.get(root.children.indexOf(leaf)));
			} else {
				root.children
						.add(buildTreeFromFolderList(String.join("/", subPaths.subList(1, subPaths.size())), leaf));
			}
		}
		return root;
	}

}
