package com.snapsoft;

import java.util.Arrays;

import org.junit.Test;

public class TreeItemTest {

	private static final String[] writableFolders = { "/var/opt/gitlab", "/var/lib/jenkins",
			"/var/opt/gitlab/gitlab-workhorse/socket", "/etc/log/nginx" };

	private static final String[] readableFolders = { "/var", "/var/lib", "/var/lib/jenkins", "/var/opt",
			"/var/opt/gitlab", "/var/opt/gitlab/git-data/", "/var/opt/gitlab/gitlab-workhorse/socket", "/etc/log" };

	@Test
	public void test() {
		TreeItem root = TreeItem.getWritableFolderStructure(Arrays.asList(readableFolders),
				Arrays.asList(writableFolders));
		System.out.println(TreeItem.toJSON(root, root.children).toString(2));
	}
}
